<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login','AuthController@login');
Route::apiResource('users', 'UsersController');
Route::apiResource('clients', 'ClientsController');
Route::apiResource('categories', 'CategoriesController');
Route::apiResource('books', 'BooksController');
Route::post('/books/borrowed/{id}', 'BooksController@borrowed');
Route::post('/books/return/{id}', 'BooksController@returnBook');
Route::get('/books/search/{name}', 'BooksController@search');
