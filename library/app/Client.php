<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $fillable = [
      'name',
      'age'
    ];

    public function getAvatarAttribute ($value)
    {
        if($value === null){
            return null;
        }else {
            $link = str_replace("public", "", $value);
            return 'storage' . $link;
        }
    }

    public function books ()
    {
        return $this->hasMany('App\Book');
    }
}
