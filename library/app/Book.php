<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'author',
        'published_date',
        'user_id',
        'category_id'
    ];

    public function category ()
    {
        return $this->belongsTo('App\Category');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
