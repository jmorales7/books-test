<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\Categories\StoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = Category::paginate(5);

            return response()->json([
                'data' => $data,
                'status' => 'success'
            ],200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ],500);

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {

            $category = Category::create($request->all());

            return response()->json([
                'data' => $category,
                'message' => 'success'
            ],200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ],500);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        try {

            return response()->json([
                'data' => $category,
                'message' => 'success'
            ],200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ],500);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        try {

            $category->name = $request->name;
            $category->description = $request->description;
            $category->save();

            return response()->json([
                'data' => $category,
                'message' => 'success'
            ],200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ],500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        try {

            $category->delete();

            return response()->json([
                'data' => $category,
                'message' => 'success'
            ],200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ],500);

        }
    }
}
