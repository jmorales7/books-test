<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests\Client\StoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = Client::with('books')->paginate(10);
            return response()->json([
                'data' => $data,
                'satatus' => 'success'
            ], 200);
        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {

            $client = Client::create($request->all());

            if($request->hasFile('avatar')) {

                $path = $request->file('avatar')->store('public/clients/avatar');
                $client->avatar = $path;
                $client->save();

            }
            return response()->json([
                'data' => $client,
                'satatus' => 'success'
            ], 200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);
        }

    }
    /**
     * Display the specified resource.
     *
     * @param  object  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        try {

            return response()->json([
                'data' => $client,
                'satatus' => 'success'
            ], 200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        try {

            $client->name = $request->name;
            $client->age = $request->age;

            if($request->hasFile('avatar')) {
                if($client->avatar != null) {
                    Storage::delete($client->avatar);
                    $path = $request->file('avatar')->store('public/clients/avatar');
                    $client->avatar = $path;

                } else {
                    $path = $request->file('avatar')->store('public/clients/avatar');
                    $client->avatar = $path;
                }

            }
            $client->save();

            return response()->json([
                'data' => $client,
                'satatus' => 'success'
            ], 200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  object  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        try {

            if($client->avatar != null) {
                Storage::delete($client->avatar);
            }

            $client->delete();

            return response()->json([
                'data' => $client,
                'satatus' => 'success'
            ], 200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);

        }
    }
}
