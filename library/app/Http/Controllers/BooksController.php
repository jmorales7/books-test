<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Requests\Book\StoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $data = Book::with(['client:id,name','category:id,name'])->paginate(5,['id','name','author','published_date','client_id','category_id']);

            return response()->json([
                'data' => $data,
                'status' => 'success'
            ],200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ],500);

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            $book = new Book();
            $book->name = $request->name;
            $book->author = $request->author;
            $book->published_date = $request->published_date;
            $book->category_id = $request->category_id;

            $book->save();

            return response()->json([
                'data' => $book,
                'status' => 'success'
            ],200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ],500);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  object  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        try {

            return response()->json([
                'data' => $book,
                'status' => 'success'
            ],200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ],500);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        try {

            $book->name = $request->name;
            $book->author = $request->author;
            $book->published_date = $request->published_date;
            $book->category_id = $request->category_id;

            $book->save();
            $book->client;
            $book->category;

            return response()->json([
                'data' => $book,
                'status' => 'success'
            ],200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ],500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  object  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        try {
           $book->delete();

            return response()->json([
                'data' => $book,
                'status' => 'success'
            ],200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ],500);

        }
    }

    /**
     * Add the user who borrowed a book.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function borrowed(Request $request, $id)
    {
        try {
            $book = Book::findOrFail($id);
            $book->client_id = $request->client_id;

            $book->save();
            $book->client;
            $book->category;

            return response()->json([
                'data'=> $book,
                'status' => 'success'
            ],200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ],500);

        }


    }

    public function returnBook(Request $request, $id)
    {
        try {
            $book = Book::findOrFail($id);
            $book->client_id = null;

            $book->save();
            $book->category;


            return response()->json([
                'data' => $book,
                'status' => 'success'
            ], 200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);

        }
    }

    public function search(Request $request)
    {
        try {
            $book = Book::with(['client:id,name','category:id,name'])
                        ->where('name','like',"%$request->name%")->paginate(5,['id','name','author','published_date','client_id','category_id']);

            return response()->json([
                'data' => $book,
                'status' => 'success'
            ], 200);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);

        }
    }


}
