<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    public function login(LoginRequest $request){



        try {
            $http = new \GuzzleHttp\Client;

            $response = $http->post('http://localhost:8001/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => '2',
                    'client_secret' => 'vpaDZWnY7j3fJkCYLiPNJJ8uzrIFxB49GGWB8xph',
                    'username' => $request->email,
                    'password' => $request->password,
                    'scope' => '*',
                ],
            ]);

            $data = $response->getBody();

            return  response($data, 200);

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if ($e->getCode() === 400) {
                Log::error($e->getMessage());
                return response()->json(['message' => 'Please enter email and password.'], $e->getCode());
            } else if ($e->getCode() === 401) {
                Log::error($e->getMessage());
                return response()->json(['message' => 'Your credentials are incorrect. Please try again'], $e->getCode());
            }
            Log::error($e->getMessage());
            return response()->json(['message' => 'Something is bad, contact with the administrator'], $e->getCode());
        }


    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('Success exit', 200);
    }

}
