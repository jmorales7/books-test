<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = User::paginate(5);
            return response()->json([
                'data' => $data,
                'satatus' => 'success'
            ], 200);
        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            $user = User::create($request->all());
            if($request->hasFile('avatar')) {
                $path = $request->file('avatar')->store('public/users/avatar');
                $user->avatar = $path;
                $user->save();
            }

            return response()->json([
                'data' => $user,
                'satatus' => 'success'
            ], 200);
        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  object  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        try {

            return response()->json([
                'data' => $user,
                'satatus' => 'success'
            ], 200);
        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  obeject  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,User $user)
    {
        try {
            $user->name = $request->name;
            $user->email = $request->email;

            if($request->hasFile('avatar')) {
                if($user->avatar != null) {
                    Storage::delete($user->avatar);
                    $path = $request->file('avatar')->store('public/users/avatar');
                    $user->avatar = $path;

                } else {
                    $path = $request->file('avatar')->store('public/users/avatar');
                    $user->avatar = $path;
                }

            }

            $user->save();

            return response()->json([
                'data' => $user,
                'satatus' => 'success'
            ], 200);
        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  object  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            if($user->avatar != null) {
                Storage::delete($user->avatar);
            }

            $user->delete();

            return response()->json([
                'data' => $user,
                'satatus' => 'success'
            ], 200);
        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'faild'
            ], 500);
        }
    }
}
