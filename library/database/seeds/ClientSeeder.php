<?php

use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'name' => 'Rogers',
            'Age' => 22
        ]);
        DB::table('clients')->insert([
            'name' => 'Peter Parker',
            'Age' => 23
        ]);
        DB::table('clients')->insert([
            'name' => 'Goku',
            'Age' => 24
        ]);
        DB::table('clients')->insert([
            'name' => 'Katy',
            'Age' => 25
        ]);
        DB::table('clients')->insert([
            'name' => 'Bruce Wayne',
            'Age' => 21
        ]);
        DB::table('clients')->insert([
            'name' => 'Norman',
            'Age' => 22
        ]);
        DB::table('clients')->insert([
            'name' => 'Tony Stark',
            'Age' => 22
        ]);
        DB::table('clients')->insert([
            'name' => 'Justin',
            'Age' => 28
        ]);
        DB::table('clients')->insert([
            'name' => 'Frank',
            'Age' => 29
        ]);
        DB::table('clients')->insert([
            'name' => 'Naruto',
            'Age' => 28
        ]);
        DB::table('clients')->insert([
            'name' => 'Sasuke',
            'Age' => 30
        ]);
        DB::table('clients')->insert([
            'name' => 'Gohan',
            'Age' => 20
        ]);
        DB::table('clients')->insert([
            'name' => 'Sakura',
            'Age' => 28
        ]);
        DB::table('clients')->insert([
            'name' => 'Luffy',
            'Age' => 28
        ]);
        DB::table('clients')->insert([
            'name' => 'Neymar',
            'Age' => 28
        ]);
    }
}
