<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Science Fiction',
            'description' => 'Content on the possible consequences of foreseeable or hypothetical scientific and technical achievements and inventions.'
        ]);
         DB::table('categories')->insert([
             'name' => 'Novel',
             'description' => 'Prose narration, usually extensive'
         ]);
        DB::table('categories')->insert([
            'name' => 'Terror',
            'description' => 'It is defined by the sensation it causes: fear'
        ]);
        DB::table('categories')->insert([
            'name' => 'Romance',
            'description' => "Love's stories"
        ]);
        DB::table('categories')->insert([
            'name' => 'Adventures',
            'description' => "Adventure stories"
        ]);
        DB::table('categories')->insert([
            'name' => 'Childish',
            'description' => "Children's storiess"
        ]);
    }
}
