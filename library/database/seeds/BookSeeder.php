<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            'name' => 'Lord of the Rings',
            'author' => 'J. R. R. Tolkien',
            'published_date' => Carbon::now(),
            'category_id' => 1
        ]);
        DB::table('books')->insert([
            'name' => 'Harry Poter',
            'author' => 'J. K. Rowling',
            'published_date' => Carbon::now(),
            'category_id' => 1
        ]);
        DB::table('books')->insert([
            'name' => 'The Hunger Games',
            'author' => 'Suzanne Collins',
            'published_date' => Carbon::now(),
            'category_id' => 1
        ]);
        DB::table('books')->insert([
            'name' => 'The Kite Runner',
            'author' => 'Khaled Hosseini',
            'published_date' => Carbon::now(),
            'category_id' => 2
        ]);
        DB::table('books')->insert([
            'name' => 'Number the Stars',
            'author' => 'Lois Lowry',
            'published_date' => Carbon::now(),
            'category_id' => 2
        ]);
        DB::table('books')->insert([
            'name' => 'Pride and Prejudice',
            'author' => 'Jane Austen',
            'published_date' => Carbon::now(),
            'category_id' => 2
        ]);
        DB::table('books')->insert([
            'name' => 'The Bad Seed',
            'author' => 'William March',
            'published_date' => Carbon::now(),
            'category_id' => 3
        ]);
        DB::table('books')->insert([
            'name' => 'At the Mountains of Madness',
            'author' => 'H.P. Lovecraft',
            'published_date' => Carbon::now(),
            'category_id' => 3
        ]);
        DB::table('books')->insert([
            'name' => 'Beloved',
            'author' => 'Toni Morrison',
            'published_date' => Carbon::now(),
            'category_id' => 3
        ]);
        DB::table('books')->insert([
            'name' => 'Toward That Which Is Beautiful',
            'author' => 'Marian Wernicke',
            'published_date' => Carbon::now(),
            'category_id' => 4
        ]);
        DB::table('books')->insert([
            'name' => 'When Morning Comes ',
            'author' => 'A.M. Wilson ',
            'published_date' => Carbon::now(),
            'category_id' => 4
        ]);
        DB::table('books')->insert([
            'name' => 'When Morning Comes ',
            'author' => 'A.M. Wilson ',
            'published_date' => Carbon::now(),
            'category_id' => 4
        ]);

    }
}
