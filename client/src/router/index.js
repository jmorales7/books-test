import Vue from 'vue'
import VueRouter from 'vue-router'
import UserIndex from '../views/users/index'
import UserCreate from '../views/users/create'
import ClientIndex from '../views/clients/index'
import ClientCreate from '../views/clients/create'
import CategoryIndex from '../views/categories/index'
import CategoryCreate from '../views/categories/create'
import BookIndex from '../views/books/index'
import BookCreate from '../views/books/create'
import Login from '@/components/auth/login'
import Home from '@/views/Home'




Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/users',
    name: 'UserIndex',
    component: UserIndex,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/users/create',
    name: 'UserCreate',
    component: UserCreate,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/clients',
    name: 'ClientIndex',
    component: ClientIndex,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/clients/create',
    name: 'ClientCreate',
    component: ClientCreate,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/categories',
    name: 'CategoryIndex',
    component: CategoryIndex,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/categories/create',
    name: 'CategoryCreate',
    component: CategoryCreate,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/books',
    name: 'BookIndex',
    component: BookIndex,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/books/create',
    name: 'BookCreate',
    component: BookCreate,
    meta: {
      requiresAuth: true
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})



router.beforeEach((to, from, next) => {

  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('token') == null) {
      next({
        path: '/login',
      })
    } else
      next()
  } else {
    next()
  }
})






export default router
