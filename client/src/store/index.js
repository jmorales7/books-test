import Vue from 'vue'
import Vuex from 'vuex'
import User from './modules/user'
import Client from './modules/clients'
import Category from './modules/categories'
import Book from './modules/books'



Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    authUser: localStorage.user || null,
    token: localStorage.token
  },
  getters: {
    getAuthUser: state => state.authUser,
    getToken: state => state.token
  },
  mutations: {
    setAuthUser: (state,user) => {
      state.authUser = user
    },
    setToken: (state, token) => {
      localStorage.removeItem('token')
      state.authUser = token
    },
    removeAuthUser(state) {
      localStorage.removeItem('user')
      state.authUser = null

    }
  },
  actions: {

  },
  modules: {
    User,
    Client,
    Category,
    Book
  }
})
