
import axios from 'axios';

const state = {
  categories: [],
  category: ''

};

const getters = {
  allCategories: state => state.categories,
  getCategory: state => state.category
};

const actions = {
  async fetchCategory({commit},category) {
    commit('setCategory',category)
  },
  async fetchCategories({ commit },categories) {
    commit('setCategories', categories);
  },
  async deleteCategory({ commit }, id) {
    await axios.delete(`http://localhost:8000/api/categories/${id}`);
    commit('removeCategory', id);
  },
  async updateCategory({ commit }, category) {
      const fd = new FormData()
          
      fd.append('name',category.name)
      fd.append('description',category.description)
      fd.append('_method','put')
    const response = await axios.post(
      `http://localhost:8000/api/categories/${category.id}`,fd);

    commit('updateCategory', response.data.data);
  }
};

const mutations = {
  setCategory: (state,category ) => state.category = category,
  setCategories: (state, categories) => (state.categories = categories),
  removeCategory: (state, id) =>
    (state.categories = state.categories.filter(category => category.id !== id)),

  updateCategory: (state, categoryUpdate) => {
    const index = state.categories.findIndex(category => category.id === categoryUpdate.id);
    if (index !== -1) {
      state.categories.splice(index, 1, categoryUpdate);
    }
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};