
import axios from 'axios';

const state = {
  books: [],
  book: ''

};

const getters = {
  allBooks: state => state.books,
  getBook: state => state.book
};

const actions = {
  async fetchBook({commit},book) {
    commit('setBook',book)
  },
  async fetchBooks({ commit },books) {
    commit('setBooks', books);
  },
  async deleteBook({ commit }, id) {
    await axios.delete(`http://localhost:8000/api/books/${id}`);
    commit('removeBook', id);
  },
  async updateBook({ commit }, book) {
      const fd = new FormData()
      fd.append('name',book.name)
      fd.append('author',book.author)
      fd.append('published_date',book.published_date)
      fd.append('client_id',book.client_id)
      fd.append('category_id',book.category_id)
      fd.append('_method','put')
    const response = await axios.post(
      `http://localhost:8000/api/books/${book.id}`,fd);

    commit('updateBook', response.data);
  },
  async giveBackBook({ commit },book) {
      const response = await axios.post(`http://localhost:8000/api/books/return/${book.id}`)
      commit('updateBook', response.data);
  },
  async borrowedBook({ commit },book) {
      const response = await axios.post(`http://localhost:8000/api/books/borrowed/${book.id}`,{client_id: book.client_id})
      commit('updateBook', response.data);
    }
};

const mutations = {
  setBook: (state,book ) => state.book = book,
  setBooks: (state, books) => (state.books = books),
  removeBook: (state, id) =>
    (state.books = state.books.filter(book => book.id !== id)),

  updateBook: (state, bookUpdate) => {

      const index = state.books.findIndex(book => book.id === bookUpdate.data.id);

      if (index !== -1) {
        state.books.splice(index, 1, bookUpdate.data);
    }

  }
};

export default {
  state,
  getters,
  actions,
  mutations
};