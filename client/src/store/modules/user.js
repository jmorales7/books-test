
import axios from 'axios';

const state = {
  users: [],
  user: ''

};

const getters = {
  allUsers: state => state.users,
  getUser: state => state.user
};

const actions = {
  async fetchUser({commit},user) {
    commit('setUser',user)
  },
  async fetchUsers({ commit }, users) {
    
    commit('setUsers', users);
  },
  async deleteUser({ commit }, id) {
    await axios.delete(`http://localhost:8000/api/users/${id}`);
    commit('removeUser', id);
  },
  async updateUser({ commit }, user) {
      const fd = new FormData()
      fd.append('name',user.name)
      fd.append('email',user.email)
      fd.append('avatar',user.avatar)
      fd.append('_method','put')
    const response = await axios.post(
      `http://localhost:8000/api/users/${user.id}`,fd);

    commit('updateUser', response.data.data);
  }
};

const mutations = {
  setUser: (state,user ) => state.user = user,
  setUsers: (state, users) => (state.users = users),
  removeUser: (state, id) => {
    state.users = state.users.filter(user => user.id !== id)
  },

  updateUser: (state, userUpdate) => {
    const index = state.users.findIndex(user => user.id === userUpdate.id);
    if (index !== -1) {
      state.users.splice(index, 1, userUpdate);
    }
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};