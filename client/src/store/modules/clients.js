
import axios from 'axios';

const state = {
  clients: [],
  client: ''

};

const getters = {
  allClients: state => state.clients,
  getClient: state => state.client
};

const actions = {
  async fetchClient({commit},client) {
    commit('setClient',client)
  },
  async fetchClients({ commit },clients) {
    commit('setClients', clients);
  },
  async deleteClient({ commit }, id) {
    await axios.delete(`http://localhost:8000/api/clients/${id}`);
    commit('removeClient', id);
  },
  async updateClient({ commit }, client) {
      const fd = new FormData()
          
      fd.append('name',client.name)
      fd.append('age',client.age)
      fd.append('avatar',client.avatar)
      fd.append('_method','put')
    const response = await axios.post(
      `http://localhost:8000/api/clients/${client.id}`,fd);

    commit('updateClient', response.data.data);
  }
};

const mutations = {
  setClient: (state,client ) => state.client = client,
  setClients: (state, clients) => (state.clients = clients),
  removeClient: (state, id) =>
    (state.clients = state.clients.filter(client => client.id !== id)),

  updateClient: (state, clientUpdate) => {
    const index = state.clients.findIndex(client => client.id === clientUpdate.id);
    if (index !== -1) {
      state.clients.splice(index, 1, clientUpdate);
    }
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};